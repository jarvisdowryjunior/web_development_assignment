****************************************************************MVC SUPPORTING AND NON SUPPORTING FRAMEWORK**********************************************************************************

------------------MVC SUPPORTING------------------

1.ASP.NET MVC

ASP.NET supports three major development models: Web Pages, Web Forms and MVC (Model View Controller). ASP.NET MVC framework is a lightweight, highly testable presentation framework that is integrated with the existing ASP.NET features, such as master pages, authentication, etc. Within .NET, this framework is defined in the System.Web.Mvc assembly. The latest version of the MVC Framework is 5.0. We use Visual Studio to create ASP.NET MVC applications which can be added as a template in Visual Studio.

2.BackBone.js

Backbone.js offers a flexible, minimalist solution to separating concerns in your application. As a consequence of its minimal solution, Backbone.js used without its own plugins is more of a utility library than a fully-fledged MVC framework.It may appear that Backbone isn�t as fully featured as the other popular MVC frameworks available. Pairing Backbone with one of its add-ons like Marionette or Chaplin ensures that Backbone.js is as feature complete as other frameworks.

3.Angular

Angular.js is designed and built by Google and is quickly gaining popularity. The stand out feature of Angular is its use of custom HTML tags and components to specify the intentions of your application.It provides a HTML compiler that allows users to create their own domain specific language; this can be an extremely powerful tool. The approach is different than other frameworks which seek to deal with HTML�s shortcomings by abstracting away the HTML, CSS and JavaScript by providing alternative ways to manipulate the DOM.

4.Ember

EmberJS is an opinionated, modular framework that can be simple and intuitive to work with if you follow its guidelines on how an application is built the Ember way.Its convention over configuration approach means it provides a good starting point to begin construction when compared to other frameworks.

5.Laravel

The framework�s main goal is to take the drag from development to make it more fun and enjoyable. Laravel�s philosophy suits very well mostly among the millennial web developers.
Laravel achieved this output by easing common tasks like authentication, routing, sessions, and caching. It is pleasing even without sacrificing functionality � making it very suitable for beginners of PHP.

6.Code Ignitor

CodeIgniter is one of the oldest PHP frameworks, a brainchild of Rick Ellis in 2006. Being a classic doesn�t imply that this framework is outdated. It only means that CodeIgniter has gone through a lot of trials, errors, and updates that made it a PHP MVC framework.

7.Spring

The Spring Web MVC framework provides Model-View-Controller (MVC) architecture and ready components that can be used to develop flexible and loosely coupled web applications. The MVC pattern results in separating the different aspects of the application (input logic, business logic, and UI logic), while providing a loose coupling between these elements.

8. React

Facebook and Instagram rely on the React JavaScript framework due to its ability to build large applications that are dynamic and can be vastly scaled. React is at its best when rendering complex user interfaces and is rapidly becoming the fastest growing JavaScript framework of the modern age.

9. Aurelia

A relatively young framework, Aurelia was originally released in January 2015 and can trace its roots back to AngularJS. It is notable for its vast collection of small libraries from which developers can pick and choose without having to implement the entire framework.

10. Polymer
Another Google invention, the Polymer framework extends the capabilities of HTML code by using web components. It means that standard HTML5 elements can be customised (for example, <audio> could become <my-audio>).

-----------------------------------MVC NON SUPPORTING TECHNOLOGIES------------------------

1.Django:

Django follows MVC pattern very closely but it uses slightly different terminology. Django is essentially an MTV (Model-Template-View) framework. Django uses the term Templates for Views and Views for Controller. In other words, in Django views are called templates and controllers are called views. Hence our HTML code will be in templates and Python code will be in views and models.